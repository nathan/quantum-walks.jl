### A Pluto.jl notebook ###
# v0.12.19

using Markdown
using InteractiveUtils

# ╔═╡ ea1f3ff4-609c-11eb-33bc-5d61b6c33e53
begin
	using LinearAlgebra
	using LightGraphs
	using Plots
	using Printf
end

# ╔═╡ fd188594-60e1-11eb-31dd-fb58e5249a11
md"""
# Visualizing quantum walks on small graphs

This page has all the Julia code necessary for creating the images; as such, it must begin with a couple of trivialities, as loading the dependencies and defining the adjacency matrix of a graph.
"""

# ╔═╡ 591621be-609f-11eb-280b-d1f906bf82a5
begin
	
	function adjacency_matrix(n :: Int64, e :: AbstractEdge)
		f(i :: Int64) = [ convert(Float64, i == j) for j in 1:n ]
		f(src(e)) .* f(dst(e))' + f(dst(e)) .* f(src(e))'
	end
	
	function adjacency_matrix(G :: SimpleGraph)
		f(e) = adjacency_matrix(nv(G), e)
		sum(f.(edges(G)))
	end

end

# ╔═╡ cd19fc7a-60d6-11eb-0237-d392200b2d08
md"""
# Quantum Walk representation

I am representing a quantum walk as the adjacency matrix of the graph.
"""

# ╔═╡ ca433058-60ad-11eb-1bef-7d57f7550068
begin
	struct QuantumWalk
		A :: Matrix
	end
	
	function QuantumWalk(G :: SimpleGraph)
		QuantumWalk(adjacency_matrix(G))
	end
end

# ╔═╡ 719e7aa6-60db-11eb-221a-03a7f0f800b4
md"""
I also created a custom `heatmap` function for displaying the matrices.
"""

# ╔═╡ a9edbeac-60d7-11eb-15f5-4f40db8112bb
function heatmap(A :: Matrix; kargs...)
	Plots.heatmap(A;
			clim=(0.0, 1.0),
			framestyle = :none,
			ticks = false,
			aspectratio = :equal,
			c = :thermal,
			kargs...
		)
end

# ╔═╡ 378427f0-60de-11eb-1129-8fd539a2cb79
md"""
Then it is easy to create an animation:
Just iterate over a bunch of times and display the component-wise absolute value of $\exp(itA)$.
"""

# ╔═╡ e70179e6-60be-11eb-050c-8fb75ae0abad
function animate(walk :: QuantumWalk, t0 :: Float64, t1 :: Float64)
	step = (t1 - t0)/200
	animation = @animate for t in t0:step:t1
		heatmap(abs.(exp(im * t * walk.A)), title= @sprintf("t = %.2f", t),)
	end
	gif(animation, fps=10)
end

# ╔═╡ a32ef6ac-60ab-11eb-3c82-918a2f6d08da
gr()

# ╔═╡ 787e58fc-60de-11eb-2ba1-1bcbcaccf680
md"""
# Visualizations

I can now display some quantum walks.
I will show for $P_2$ and for $P_3$ at their respective periods.
"""

# ╔═╡ 21d2f412-60c6-11eb-240c-31914c836d6b
animate(QuantumWalk(path_graph(2)), 0.0, 2.0 * pi)

# ╔═╡ 32cc51e8-60b5-11eb-033e-99089df4834e
animate(QuantumWalk(path_graph(3)), 0.0, sqrt(2) * pi)

# ╔═╡ a4e50fb2-60de-11eb-0d37-930742dd193e
md"""
We can also observe the instantaneous uniform mixing of the claw graph at

$$t = \frac{2\pi}{3\sqrt{3}} \approx 1.21$$.
"""

# ╔═╡ f27d4730-60a2-11eb-1729-954ba72a95ef
begin
	claw_graph = SimpleGraph(4)
	for i in 2:4
		add_edge!(claw_graph, 1, i)
	end
	animate(QuantumWalk(claw_graph), 0.0, 4.0 * pi / (3 * sqrt(3)))
end

# ╔═╡ c0aa0644-60de-11eb-3fdd-b9342bba0564
md"""
# Cartesian product

There are two interesting things to do with the cartesian product.
The first one is to watch how the quantum walk on the cartesian product reduces to the kronecker product in each matrix.
"""

# ╔═╡ ca5afb32-60d0-11eb-09cc-7960fa401806
function visualize_product(G :: SimpleGraph, H :: SimpleGraph, t0 :: Float64, t1 :: Float64)
	step = (t1 - t0)/200
	custom_heatmap(graph, t) =
		heatmap(abs.(exp(im * t * adjacency_matrix(graph))), colorbar = false, title = "")
	animation = @animate for t in t0:step:t1
		plot(
			custom_heatmap(cartesian_product(G, H), t),
			custom_heatmap(H, t),
			custom_heatmap(G, t),
			layout=3,
			plot_title = "t = $(t)"
		)
	end
	gif(animation, fps=10)
end

# ╔═╡ 6982a048-60ce-11eb-3545-8d4347c76a68
visualize_product(path_graph(2), path_graph(2), 0.0, 2 * pi)

# ╔═╡ 461fa866-60d4-11eb-399d-4b5e2178bf18
visualize_product(path_graph(3), path_graph(3), 0.0, sqrt(2) * pi)

# ╔═╡ 5e70a5b4-60d4-11eb-12a1-6db6d7a11577
visualize_product(path_graph(2), path_graph(3), 0.0, 2.0 * pi)

# ╔═╡ 0e386bd0-60df-11eb-3fd4-b587b01af9bf
md"""
We may also consider the cartesian powers of a graph, and visualize the behavior of the quantum walks on them.
"""

# ╔═╡ e2c8b9ea-60c6-11eb-1dbb-af00244be41b
function cartesian_power(G :: SimpleGraph, k :: Int64)
	if k == 0
		SimpleGraph(1)
	else
		cartesian_product(G, cartesian_power(G, k - 1))
	end
end

# ╔═╡ a64dadc2-60c6-11eb-0899-0bee6b79f458
animate(QuantumWalk(cartesian_power(path_graph(2), 2)), 0.0, 2.0 * pi)

# ╔═╡ e61dcb58-60cb-11eb-25f3-3dcc2f22f8b3
animate(QuantumWalk(cartesian_power(path_graph(3), 4)), 0.0, sqrt(2) * pi)

# ╔═╡ Cell order:
# ╟─fd188594-60e1-11eb-31dd-fb58e5249a11
# ╠═ea1f3ff4-609c-11eb-33bc-5d61b6c33e53
# ╠═591621be-609f-11eb-280b-d1f906bf82a5
# ╟─cd19fc7a-60d6-11eb-0237-d392200b2d08
# ╠═ca433058-60ad-11eb-1bef-7d57f7550068
# ╟─719e7aa6-60db-11eb-221a-03a7f0f800b4
# ╠═a9edbeac-60d7-11eb-15f5-4f40db8112bb
# ╟─378427f0-60de-11eb-1129-8fd539a2cb79
# ╠═e70179e6-60be-11eb-050c-8fb75ae0abad
# ╟─a32ef6ac-60ab-11eb-3c82-918a2f6d08da
# ╟─787e58fc-60de-11eb-2ba1-1bcbcaccf680
# ╠═21d2f412-60c6-11eb-240c-31914c836d6b
# ╠═32cc51e8-60b5-11eb-033e-99089df4834e
# ╟─a4e50fb2-60de-11eb-0d37-930742dd193e
# ╠═f27d4730-60a2-11eb-1729-954ba72a95ef
# ╟─c0aa0644-60de-11eb-3fdd-b9342bba0564
# ╠═ca5afb32-60d0-11eb-09cc-7960fa401806
# ╠═6982a048-60ce-11eb-3545-8d4347c76a68
# ╠═461fa866-60d4-11eb-399d-4b5e2178bf18
# ╠═5e70a5b4-60d4-11eb-12a1-6db6d7a11577
# ╟─0e386bd0-60df-11eb-3fd4-b587b01af9bf
# ╠═e2c8b9ea-60c6-11eb-1dbb-af00244be41b
# ╠═a64dadc2-60c6-11eb-0899-0bee6b79f458
# ╠═e61dcb58-60cb-11eb-25f3-3dcc2f22f8b3
